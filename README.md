# EXPLANATION AND REQUIREMENTS

This is an evaluable exercise for the **POSTGRADUATE COURSE IN SOFTWARE QUALITY ASSURANCE**

This automation is been done with Cypress.

In order to execute those tests is requiered Node.js 12, 14 or above.
Remember to **npm install** in order to get all the requiered dependencies for the project.

This project includes two scenarios for buying two different products at http://sqademosatp.net/catalog/mosatp.

Tests are stored at **cypress\e2e**.

To improve the maintainability of the project every test file will include as less logic as possible. Every kind of logic should be written into support folder. If a functions is going to be repeated multiple times is recommended to store it as a custom command at **support\commands.js**.

Also to improve this maintainability, and because constant data can change in the future. A **constants.js** file has been created on **support** folder.

# EXECUTING TESTS

Tests can be executed headless or GUI.

-   To execute tests in headless mode you can use the commander line to run: **npx run cypress run**

-   To execute tests in GUI mode you can use the commander line to open cypress: **npx cypress open**

# GUI MODE

In GUI mode the Cypress application opens.

![alt text](img/1.jpg 'Cypress')

And you can see all the Test Cases for the project.
In this case we only have **test-case-1** and **test-case-2**.
If you click one, you will execute the automation test and you will be able to see the logs and the execution in real time.

![alt text](img/2.jpg 'Execution of Test Suite')

### Example of a test passing fine:

![Video of test running](cypress/videos/test-case-1.cy.js.mp4)

# HEADLESS MODE

This is the best mode if want to integrate with pipelines since is faster and doesn't requiere a visual interface.

![alt text](img/3.jpg 'Cypress in Headless mode')

# REPORT

At the end of the execution a report will be generated/updated at **reports** folder as an html file.

![alt text](img/4.jpg 'Report')
