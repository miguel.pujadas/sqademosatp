const { defineConfig } = require('cypress');

module.exports = defineConfig({
    reporter: 'cypress-mochawesome-reporter',
    reporterOptions: {
        charts: true,
        reportPageTitle: 'custom-title',
        embeddedScreenshots: true,
        inlineAssets: true,
        saveAllAttempts: false
    },
    e2e: {
        baseUrl: 'http://sqademosatp.net/catalog/',
        setupNodeEvents(on, config) {
            require('cypress-mochawesome-reporter/plugin')(on);
        }
    },
    screenshotsFolder: 'reports/images',
    retries: {
        runMode: 1,
        openMode: 1
    },
    watchForFileChanges: true
});
