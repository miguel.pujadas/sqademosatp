import { userCredentials, paymentMethods, messages } from '../support/constants';

describe('TEST CASE 2', () => {
    it('buy SBeloved', () => {
        cy.searchProduct('Beloved');
        cy.selectQuantity('3');
        cy.login(userCredentials.userName, userCredentials.password);
        cy.proceed();
        cy.selectPaymentMode(paymentMethods.creditCard);
        cy.confirmPayment();
        cy.get('h1').should('have.text', messages.confirmationMessage);
    });
});
