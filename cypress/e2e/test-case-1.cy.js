import { userCredentials, paymentMethods, messages } from '../support/constants';

describe('TEST CASE 1 sketch', () => {
    it('buy Samsung Galaxy Tab', () => {
        cy.searchProduct('Samsung Galaxy Tab');
        cy.selectQuantity('2');
        cy.login(userCredentials.userName, userCredentials.password);
        cy.proceed();
        cy.selectPaymentMode(paymentMethods.creditCard);
        cy.confirmPayment();
        cy.get('h1').should('have.text', messages.confirmationMessage);
    });
});
