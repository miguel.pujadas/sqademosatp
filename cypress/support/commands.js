Cypress.Commands.add('login', (email, pw) => {
    cy.log('LOGIN');
    cy.get('[name="email_address"]').type(email);
    cy.get('[name="password"]').type(pw);
    cy.get('#tdb1').click();
});

Cypress.Commands.add('searchProduct', (product) => {
    cy.log('SELECT PRODUCT');
    cy.get('[name="keywords"]').type(product).type('{enter}');
    cy.get('#tdb4').click();
});

Cypress.Commands.add('selectQuantity', (quantity) => {
    cy.log('SELECT QUANTITY');
    cy.get('[name="cart_quantity[]"]').clear().type(quantity).type('{enter}');
    cy.get('#tdb5').click();
});

Cypress.Commands.add('proceed', () => {
    cy.get('#tdb6').click();
});

Cypress.Commands.add('selectPaymentMode', (paymentMode) => {
    cy.get('[value=' + paymentMode + ']').click();
    cy.get('#tdb6').click();
});

Cypress.Commands.add('confirmPayment', () => {
    cy.log('CONFIRM PAYMENT');
    cy.get('#tdb5').click();
});
